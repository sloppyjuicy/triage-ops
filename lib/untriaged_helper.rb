# frozen_string_literal: true

require_relative 'devops_labels'

module UntriagedHelper
  include DevopsLabels::Context

  TYPE_LABELS =
    %w(
      bug
      feature
      support\ request
      tooling
      meta
      documentation
      triage\ report
    ).freeze

  def unlabelled_issues_triagers
    select_available_team_member_usernames do |data|
      data['departments']&.any? { |dept| dept.downcase == 'quality department' } &&
        !data['role']&.downcase&.include?('vice president') && !data['role']&.downcase&.include?('director') && !data['role']&.downcase&.include?('analyst')
    end
  end

  def merge_request_coaches
    select_available_team_member_usernames do |data|
      data['departments']&.any? { |dept| dept.downcase == 'merge request coach' }
    end
  end

  def distribute_items(list_items, triagers)
    puts "Potential triagers: #{triagers.inspect}"

    triagers.shuffle
      .zip(list_items.each_slice((list_items.size.to_f / triagers.size).ceil))
      .sort { |(triager_a, _), (triager_b, _)| triager_a <=> triager_b }
      .to_h
      .compact
  end

  def distribute_and_display_items_per_triager(list_items, triagers)
    items_per_triagers = distribute_items(list_items, triagers)

    text = items_per_triagers.each_with_object([]) do |(triager, items), text|
      text << "#{triager}\n\n#{items.join("\n")}"
    end.join("\n\n")

    text + "\n/assign #{items_per_triagers.keys.join(' ')}"
  end

  def untriaged?
    !triaged?
  end

  def triaged?
    has_type_label? && has_stage_label? && has_group_label?
  end

  def has_type_label?
    !type_label.nil?
  end

  def type_label
    (label_names & TYPE_LABELS).first
  end

  def select_available_team_member_usernames
    WwwGitLabCom.team_from_www.each_with_object([]) do |(username, data), memo|
      next if out_of_office?(username)

      memo << "@#{username}" if yield(data)
    end
  end

  def out_of_office?(username)
    WwwGitLabCom.roulette.find { |data| data['username'] == username }&.fetch('out_of_office', false)
  end
end
