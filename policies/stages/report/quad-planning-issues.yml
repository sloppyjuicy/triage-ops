.common_conditions: &common_conditions
  state: opened
  forbidden_labels:
    - bug
    - "quad-planning::complete-action"
    - "quad-planning::complete-no-action"
  milestone:
    - Any
  ruby: |
    labels_expected = ['workflow::ready for development', 'quad-planning::ready']
    /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }

.common_summary: &common_summary
  item: |
    - [ ] #{resource[:web_url]} {{title}} {{labels}} {{milestone}}
  summary: |
    # Group: {{title}}
    {{items}}

.common_actions: &common_actions
  label:
    - "quad-planning::ready"

resource_rules:
  issues:
    summaries:
      - name: Quad Planning Issues for Dev Section
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Dev
            summary: |
              Hi @gl-quality/dev-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              ---

              Job URL: #{ENV['CI_JOB_URL']}

              /label ~"section::dev" ~"triage report" ~Quality
              /assign @gl-quality/dev-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::source code"
            conditions:
              <<: *common_conditions
              labels:
                - "group::source code"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::source code"
          - name: Quad Planning Issues for ~"group::code review"
            conditions:
              <<: *common_conditions
              labels:
                - "group::code review"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::code review"
          - name: Quad Planning Issues for ~"group::editor"
            conditions:
              <<: *common_conditions
              labels:
                - "group::editor"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::editor"
          - name: Quad Planning Issues for ~"group::access"
            conditions:
              <<: *common_conditions
              labels:
                - "group::access"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::access"
          - name: Quad Planning Issues for ~"group::import"
            conditions:
              <<: *common_conditions
              labels:
                - "group::import"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::import"
          - name: Quad Planning Issues for ~"group::project management"
            conditions:
              <<: *common_conditions
              labels:
                - "group::project management"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::project management"
      - name: Quad Planning Issues for Ops Section
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Ops
            summary: |
              Hi @gl-quality/ci-cd-ops-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              /label ~"section::ops" ~"triage report" ~Quality
              /assign @gl-quality/ci-cd-ops-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::runner"
            conditions:
              <<: *common_conditions
              labels:
                - "group::runner"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::runner"
          - name: Quad Planning Issues for ~"group::package"
            conditions:
              <<: *common_conditions
              labels:
                - "group::package"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::package"
          - name: Quad Planning Issues for ~"group::continuous integration"
            conditions:
              <<: *common_conditions
              labels:
                - "group::continuous integration"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::continuous integration"
      - name: Quad Planning Issues for Enablement
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Enablement
            summary: |
              Hi @gl-quality/enablement-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              /label ~"section::enablement" ~"triage report" ~Quality
              /assign @gl-quality/enablement-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::memory"
            conditions:
              <<: *common_conditions
              labels:
                - "group::memory"
              ruby: |
                labels_expected = ['Deliverable', 'quad-planning::ready']
                /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::memory"
          - name: Quad Planning Issues for ~"group::geo"
            conditions:
              <<: *common_conditions
              labels:
                - "group::geo"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::geo"
          - name: Quad Planning Issues for ~"group::distribution"
            conditions:
              <<: *common_conditions
              labels:
                - "group::distribution"
              ruby: |
                labels_expected = ['Deliverable', 'quad-planning::ready']
                /^\d+\.\d+$/ =~ milestone&.title && labels_expected.any? { |label| resource[:labels].include?(label) }
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::distribution"
          - name: Quad Planning Issues for ~"group::global search"
            conditions:
              <<: *common_conditions
              labels:
                - "group::global search"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::global search"
      - name: Quad Planning Issues for Fulfillment
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Fulfillment
            summary: |
              Hi @gl-quality/fulfillment-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              /label ~"section::fulfillment" ~"triage report" ~Quality
              /assign @gl-quality/fulfillment-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::purchase"
            conditions:
              <<: *common_conditions
              labels:
                - "group::purchase"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::purchase"
          - name: Quad Planning Issues for ~"group::utilization"
            conditions:
              <<: *common_conditions
              labels:
                - "group::utilization"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::utilization"
          - name: Quad Planning Issues for ~"group::license"
            conditions:
              <<: *common_conditions
              labels:
                - "group::license"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::license"
      - name: Quad Planning Issues for Secure & Protect
        actions:
          summarize:
            destination: gitlab-org/quality/triage-reports
            title: |
              #{Date.today.iso8601} - Quad Planning Issues for Secure & Protect
            summary: |
              Hi @gl-quality/secure-protect-qe :wave:

              Please quad-plan the following issues:

              {{items}}

              /label ~"section::sec" ~"triage report" ~Quality
              /assign @gl-quality/secure-protect-qe
              /due in 8 days
        rules:
          - name: Quad Planning Issues for ~"group::composition analysis"
            conditions:
              <<: *common_conditions
              labels:
                - "group::composition analysis"
            actions:
              <<: *common_actions
              summarize:
                <<: *common_summary
                title: ~"group::composition analysis"
